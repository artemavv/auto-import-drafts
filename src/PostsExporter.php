<?php

/**
 * Class to export posts to other sites
 * @author Artem
 * 
 */
class PostsExporter {
  
    private const IS_VALID = true;
    
    private static $valid_actions = ['list_drafts', 'import_draft'];
    
    public static function validateRequest( WP_REST_Request $request ) {
        // Get query strings params from request
        $params = $request->get_query_params();

        if ( isset($params['subsite']) && absint($params['subsite']) > 0 ) {
            if ( isset($params['api_key'])) {
                $api_key = WpAutoImport::getOption('api_key');
                if ( $params['api_key'] == $api_key ) {
                    if ( isset($params['action']) ) {
                        if (in_array( $params['action'], self::$valid_actions) ) {
                            
                            // extra check for post ID
                            if ( $params['action'] == 'import_draft' ) {
                                if ( isset($params['post_to_export']) && absint($params['post_to_export']) > 0 ) {
                                    $result = self::IS_VALID;
                                }
                                else {
                                    $result = [ 400, 'Post ID is missing or invalid', 'Post ID is missing or invalid' ];
                                }
                            }
                            else {
                                $result = self::IS_VALID;
                            }
                        }
                        else {
                            $result = [ 400, 'API action is not valid', 'API action is not valid' ];
                        }
                    }
                    else {
                        $result = [ 400, 'API action is missing', 'API action is missing in request parameters' ];
                    }
                }
                else {
                    $result = [ 404, 'Post ID is missing or invalid', 'Post ID is invalid or missing in request parameters' ];
                }
            }
            else {
                $result = [ 400, 'API key is missing', 'API key is missing in request parameters' ];
            }
        }
        else {
            $result = [ 400, 'Subsite ID is missing or invalid', 'Subsite ID is invalid or missing in request parameters' ];
        }
        
        return $result;
    }
    
    public static function processRequest( WP_REST_Request $request ) {
      
        $check_result = self::validateRequest($request);
        
        if ( $check_result == self::IS_VALID ) {
          
            // Get query strings params from request
            $params = $request->get_query_params();
            
            switch ($params['action']) {
                case 'list_drafts':
                    $list = self::makeListOfAvailablePosts(absint($params['subsite']));

                    if ( is_array($list) && count($list) ) {
                        $response = [ 200, 'OK', json_encode($list) ];
                    }
                    else {
                        $response = [ 404, 'No posts found', 'No posts found on subsite ID = ' . absint($params['subsite']) ];
                    }
                    break;
                case 'import_draft':
                    
                    $response = self::exportRequestedPost(
                        absint($params['post_to_export']),
                        absint($params['subsite'])
                    );
                    
                    break;
                default:
            }
        }
        else {
            $response = $check_result;
        }
        
        $result = new WP_REST_Response(array(
            'status' => $response[0],
            'response' => $response[1],
            'body_response' => $response[2]
        ));
        
        return $result;
    }
    
    public static function makeListOfAvailablePosts( $subsite_id ) {
        global $wpdb;
        
        $where = ' post_type IN ("post","page") AND post_status IN ("publish","draft") ' ;
        $limit = 30;
        
        if ( $subsite_id == 1 ) {
            $sql = 'SELECT ID, post_title from ' . $wpdb->prefix . 'posts as p WHERE ' . $where . ' ORDER BY `post_date` DESC LIMIT ' . $limit;
        }
        else {
            $sql = 'SELECT ID, post_title from ' . $wpdb->prefix . $subsite_id . '_posts as p WHERE  ' . $where . ' ORDER BY `post_date` DESC LIMIT ' . $limit;
        }
        
        $rows = $wpdb->get_results( $sql, ARRAY_A );
		return $rows;
    }
    
    /**
     * Outputs export post data on the JSON endpoint
     */
    public static function exportRequestedPost( $post_id, $subsite_id ) {
     
        $post_to_export = self::getPostDataForExport( $post_id , $subsite_id);

        if ( $post_to_export ) {
            $result = [ 200, 'OK', json_encode($post_to_export) ];
        }
        else {
            $result = [ 404, 'Post not found', "Post with ID = $post_id not found" ];
        }

        return $result;
    }
    
    private static function getPostDataForExport( $post_id, $subsite_id ) {
		global $wpdb;
        
        if ( $subsite_id == 1 ) {
            $sql = $wpdb->prepare('SELECT * from ' . $wpdb->prefix . 'posts as p WHERE (post_type = "post" OR post_type = "page") AND ID = %d LIMIT 1', $post_id);
        }
        else {
            $sql = $wpdb->prepare('SELECT * from ' . $wpdb->prefix . $subsite_id . '_posts as p WHERE (post_type = "post" OR post_type = "page") AND ID = %d LIMIT 1', $post_id);
        }
		
		$row = $wpdb->get_row( $sql, ARRAY_A );

        if ( is_array($row) && count($row) ) {
			//$row['meta'] = self::getPostMeta( $row['ID'], $subsite_id ); -- not used
            $row['cats'] = self::getPostTaxonomy( $row['ID'], $subsite_id, 'category' );
            $row['tags'] = self::getPostTaxonomy( $row['ID'], $subsite_id, 'post_tag' );
            return $row;
		}
        else {
            return false;
        }
	}
    
    private static function getPostMeta( $post_id, $subsite_id ) {
        global $wpdb;
        
        if ( $subsite_id == 1 ) {
            $sql = $wpdb->prepare('SELECT * from ' . $wpdb->prefix . 'postmeta as pm WHERE post_id = %d', $post_id);
        }
        else { 
            $sql = $wpdb->prepare('SELECT * from ' . $wpdb->prefix . $subsite_id . '_postmeta as pm WHERE post_id = %d', $post_id);
        }
		$rows = $wpdb->get_results( $sql, ARRAY_A );
		return $rows;
    }
       
    private static function getPostTaxonomy( $post_id, $subsite_id, $taxonomy = 'post_tag' ) {
        global $wpdb;
        
        if ( $subsite_id == 1 ) {
            $prefix = $wpdb->prefix;
        }
        else {
            $prefix = $wpdb->prefix . $subsite_id . '_';
        }
        
        $table_trels = $prefix . 'term_relationships';
        $table_terms = $prefix . 'terms';
        $table_ttxes = $prefix . 'term_taxonomy';
       
        
        $sql = $wpdb->prepare(
           "SELECT tr.term_taxonomy_id, t.slug FROM $table_trels AS tr "
           . "LEFT JOIN $table_ttxes AS tax ON tr.term_taxonomy_id = tax.term_id "
           . "LEFT JOIN $table_terms AS t ON tr.term_taxonomy_id = t.term_id "
           . "WHERE tax.taxonomy = %s AND tr.object_id = %d",
           $taxonomy,
           $post_id
        );

		$rows = $wpdb->get_results( $sql, ARRAY_A );
		return $rows;
    }

}
