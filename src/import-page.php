<?php if (!defined('ABSPATH')) exit(); ?>

<div class="wrap">
    <h2><?php _e('Import Drafts', 'auto-upload-images'); ?></h2>

    <?php if (isset($import_result) && is_array($import_result) ) : ?>
      <div class="<?php echo $import_result[0]; ?>">
          <p><strong><?php echo $import_result[1]; ?></strong></p>     
      </div>
    <?php endif; ?>
    
    <?php if ( is_wp_error($available_drafts) ): ?>
        <div class="error">
          <p><strong><?php echo $available_drafts->get_error_code(); ?> :: <?php echo $available_drafts->get_error_message(); ?></strong></p>     
      </div>
    <?php endif; ?>
    
    
    <div id="import_post_from_list">
        <div id="post-body" class="metabox-holder columns-2">
            <div id="import-content" style="position: relative">
                <div class="stuffbox" style="padding: 0 20px">
                    <form method="POST">
                        <H3><?php _e('Select post to import:', 'auto-upload-images'); ?></H3>
                        <input type="hidden" name="action" value="import_draft_post_from_list"  />
                        <table class="form-table">
                            <?php foreach ( $available_drafts as $draft ): ?>
                            <tr valign="top">
                                <td>
                                    <input type="radio" name="drafts_list" value="<?php echo( $draft['ID'] ); ?>" id='drafts_list_<?php echo( $draft['ID'] ); ?>'>
                                    <label for='drafts_list_<?php echo( $draft['ID'] ); ?>'>
                                        <strong>[<?php echo( $draft['ID'] ); ?>]</strong>
                                        <?php echo( $draft['post_title'] ); ?>
                                    </label>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                        <p class="submit">
                               <?php submit_button("Import draft post", 'primary', 'submit', false); ?>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    
    <div id="import_post_by_id">
        <div id="post-body" class="metabox-holder columns-2">
            <div id="import-content" style="position: relative">
                <div class="stuffbox" style="padding: 0 20px">
                    <form method="POST">
                        <h3>Import Post</h3>
                        <input type="hidden" name="action" value="import_draft_post_by_id"  />
                        <table class="form-table">
                            <tr valign="top">
                                <th scope="row">
                                    <label for="post_id_to_import">
                                        <?php _e('Post ID to import:', 'auto-upload-images'); ?>
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="post_id_to_import" value="" class="regular-text" dir="ltr" />
                                    <p class="description"><?php _e('Enter ID of the post placed on drafts.baeldung.com', 'auto-upload-images'); ?></p>
                                </td>
                            </tr>
                        </table>
                        <p class="submit">
                               <?php submit_button("Import draft post", 'primary', 'submit', false); ?>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
</div>
