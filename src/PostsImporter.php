<?php

/**
 * Class to import posts from other sites
 * @author Artem
 * 
 */
class PostsImporter{
  
    public static $allowed_metas = false;
    
    /**
     * Gets the list of posts from the remote server
     * 
     * @return array or WP_Error
     */
    public static function fetchAvailableDrafts() {
        $request = [
            'action'    => 'list_drafts',
            'subsite'   => get_current_blog_id(),
            'api_key'   => WpAutoImport::getOption('api_key')
        ];

        $api_url = untrailingslashit(WpAutoImport::getOption('drafts_site_url')) . '/wp-json/export-post/v1/posts/?' . http_build_query($request);
        $response = wp_remote_get( $api_url, array(
            'blocking'  => true,
            'timeout'   => 15,
            'sslverify' => false,
        ));
        
        if ( ! is_wp_error( $response ) ) {
            $response = json_decode( $response['body'], ARRAY_A );
            
            if ( $response['status'] == 200 ) {
                try {
                    $response = json_decode($response['body_response'], ARRAY_A);
                }
                catch( Exception $e ) {
                    $response = new WP_Error( $e->getCode(), $e->getMessage() );
                }
            }
            else {
                $response = new WP_Error($response['status'],$response['body_response']);
            }
        }
      
        return $response;
    }
    
    
    /**
     * Imports post which is retrieved from drafts site
     * post is created on the current subsite.
     * 
     * Return two-item array: [ status, message ]
     * 
     * @param int $foreign_post_id
     * @return array
     */
    public static function importPost( $foreign_post_id ) {
        
        $response = self::fetchSinglePost($foreign_post_id);
        
        if ( ! is_wp_error( $response ) ) {
            $inserted_post_id = self::insertImportedPost($response);

            if ( ! is_wp_error( $inserted_post_id ) ) {   
                $url = htmlspecialchars(get_bloginfo('wpurl') . "/wp-admin/post.php?post=$inserted_post_id&action=edit");
                $result = [ 'updated', "Successfully imported draft post. You can view it <a href='$url'>here</a>" ];
            }
            else {
                $result = [ 'error', 'Error while importing draft post with ID = ' . $foreign_post_id
                . ';<br/> Error code: ' . $inserted_post_id->get_error_code() . ';<br/> Error message: ' . $inserted_post_id->get_error_message()  ];
            }
        }
        else {
            $result = [ 'error', 'Error while importing draft post with ID = ' . $foreign_post_id
                . ';<br/> Error code: ' . $response->get_error_code() . ';<br/> Error message: ' . $response->get_error_message()  ];
        }
        
        return $result;
    }
    
    /**
     * Fetches post data from remote server and returns post array
     * @param int $foreign_post_id
     * @return post data array or \WP_Error
     */
    public static function fetchSinglePost( $foreign_post_id ) {

        $request = [
            'action'          => 'import_draft',
            'post_to_export'  => $foreign_post_id,
            'subsite'         => get_current_blog_id(),
            'api_key'         => WpAutoImport::getOption('api_key')
        ];

        $api_url = untrailingslashit(WpAutoImport::getOption('drafts_site_url')) . '/wp-json/export-post/v1/posts/?' . http_build_query($request);
        $response = wp_remote_get( $api_url, array(
            'blocking'  => true,
            'timeout'   => 15,
            'sslverify' => false,
        ));
        
        if ( ! is_wp_error( $response ) ) {
            $response = json_decode( $response['body'], ARRAY_A );
            
            if ( $response['status'] == 200 ) {
                $response = json_decode($response['body_response'], ARRAY_A);
            }
            else {
                $response = new WP_Error($response['status'],$response['body_response']);
            }
        }
      
        return $response;
    }
    
   
    /**
     * Writes into DB (for the current subsite) the new post with postmeta; returns ID of created post
     * @param array $post
     * @return int $post_id or WP_Error
     */
    public static function insertImportedPost( $post ) {
        $inserted_post_id = false;
        global $wpdb;

		$columns = [
			'ID'					=> 'NULL',
			'post_author'			=> '%d',
			'post_date'				=> '%s',
            'post_date_gmt'         => '%s',
			'post_content'          => '%s',
			'post_title'			=> '%s',
			'post_excerpt'    		=> '%s',
			'post_status'			=> '%s',
			'comment_status'        => '%s',
			'ping_status'			=> '%s',
			'post_password'         => '%s',
			'post_name'				=> '%s',
			'to_ping'				=> '%s',
			'pinged'				=> '%s',	
			'post_modified'         => '%s',
			'post_modified_gmt'		=> '%s',
			'post_content_filtered'	=> '%s',
			'post_parent'			=> '%d',
			'guid'					=> '%s',
			'menu_order'			=> '%d',
			'post_type'				=> '%s',
			'post_mime_type'        => '%s',
			'comment_count'         => '%d'
		];
		
		
        $table_name = $wpdb->prefix . 'posts'; // if we are on subsite, $wpdb->prefix would be altered automatically by WP
        
        // the time is set to zero value to be updated later by wp_update_post() 
        // and trigger image-uploading which is done by WpAutoImport::save()
        $zero_date = '0000-00-00 00:00:00';
        
        
        if ( WpAutoImport::getOption('drafts_author_id') == 0 ) {
            $post_author = get_current_user_id();
        }
        else {
            $post_author = absint(WpAutoImport::getOption('drafts_author_id'));
        }
        
		$sql = $wpdb->prepare("INSERT INTO $table_name "
			. " (`ID`,`post_author`,`post_date`,`post_date_gmt`,`post_content`,`post_title`,`post_excerpt`,`post_status`,`comment_status`,`ping_status`,`post_password`,`post_name`,`to_ping`,`pinged`,`post_modified`,`post_modified_gmt`,`post_content_filtered`,`post_parent`,`guid`,`menu_order`,`post_type`,`post_mime_type`,`comment_count`) "
			. " VALUES ( " . implode( ',', $columns) . ") ",
			$post_author, //$post['post_author']
			$zero_date, //$post['post_date'],
			$zero_date, //$post['post_date_gmt'],
			$post['post_content'],
			$post['post_title'],
			$post['post_excerpt'],
			"draft", //$post['post_status'],
			$post['comment_status'],
			$post['ping_status'],
			$post['post_password'],
			$post['post_name'],
			$post['to_ping'],
			$post['pinged'],
			$post['post_modified'],
			$post['post_modified_gmt'],
			$post['post_content_filtered'],
			$post['post_parent'],
			$post['guid'],
			$post['menu_order'],
			$post['post_type'],
			$post['post_mime_type'],
			$post['comment_count']
		);
		
		if ( $wpdb->query( $sql ) ) {
            $inserted_post_id = $wpdb->insert_id;
            
            if ( $inserted_post_id ) {
              
                $upd_post = array(
                    'ID'            => $inserted_post_id,
                    'post_status'   => 'draft',
                    'post_date'     => $post_date,
                    'post_date_gmt' => get_gmt_from_date( $post_date ),
                );

                // set the time and also trigger image-uploading action
                wp_update_post( $upd_post );
                
                //self::importPostMeta( $inserted_post_id, $post['meta'] ); ---- not used 
                $result = self::importPostTaxonomies( $inserted_post_id, $post['cats'], $post['tags'] ); // returns either true or WP_Error
                if ( $result === true ) {
                    $result = $inserted_post_id; // no error returned so we are setting it to post ID
                }
            }
            else {
                $result = new WP_Error('306', 'Failed to insert post into DB');
            }
        }
        else {
            $result = new WP_Error('305', 'Failed to insert post into DB');
        }
		
        return $result;
    }
    

    /**
     * Inserts postmeta on current subsite
     * 
     * @param int $post_id
     * @param array $postmeta
     */
	protected static function importPostMeta( $post_id, $postmeta ) {
		
		global $wpdb;
		
		$columns = [
			'meta_id'       => 'NULL',
			'post_id'		=> '%d',
			'meta_key'		=> '%s',
			'meta_value'	=> '%s',
		];
		
        $table_name = $wpdb->prefix . 'postmeta'; // if we are on subsite, $wpdb->prefix would be altered automatically by WP
        
		$sql = $wpdb->prepare("DELETE FROM $table_name WHERE post_id = %d", $post_id);
		$wpdb->query( $sql );
        
		foreach ( $postmeta as $row ) {
            if ( self::checkIfMetaAllowed($row['meta_key']) ) {
              $sql = $wpdb->prepare("INSERT INTO $table_name "
                      . " (`meta_id`, `post_id`, `meta_key`, `meta_value`) "
                      . " VALUES ( " . implode( ',', $columns) . ") ",
                      $post_id, $row['meta_key'], $row['meta_value']);
              $wpdb->query( $sql );
            }
		}
	}
    
    
    protected static function checkIfMetaAllowed( $meta_key ) {
      
        // prepare the allowed meta keys if they are not prepared yet
        if ( self::$allowed_metas === false ) {
            self::$allowed_metas = array_map(
                'trim',
                explode("\n", WpAutoImport::getOption('include_meta'))
            );
        }
        
        // check if specified meka key matches, or if ALL meta key are allowed
        return ( in_array($meta_key, self::$allowed_metas ) || in_array( '*', self::$allowed_metas ) ) ;
    }
    
    
    protected static function importPostTaxonomies( $post_id, $cats, $tags ) {
      
        $message = '';
        $failed_cats = self::importPostTaxonomy( $post_id, $cats, "category" );
        if ( is_array($failed_cats) ) {
            $message .= 'failed to assign missing categories for this post: \'' . implode("', '", $failed_cats) . '\' . ';
        }
        
        $failed_tags = self::importPostTaxonomy( $post_id, $tags, "post_tag" );
        if ( is_array($failed_tags) ) {
            $message .= 'failed to assign missing tags for this post: \'' . implode("', '", $failed_tags) . '\' . ';
        }
        
        if ( $message) {
            return new WP_Error($post_id, "Created new draft post with ID = $post_id , but " . $message);
        }
        else {
            return true;
        }
    }
    
    /**
     * Adds taxonomy relationships for the specified  post. 
     * Uses current subsite. 
     * Checks whether this taxonomy term exists on current subsite and has the same slug as in $terms .
     * 
     * @global $wpdb
     * @param int $post_id
     * @param array $cats
     */
	protected static function importPostTaxonomy( $post_id, $terms, $taxonomy = "post_tag" ) {
		
		global $wpdb;
        $result = true; // All ok
		
        // if we are on subsite, $wpdb->prefix would be altered automatically by WP
        $table_trels = $wpdb->prefix . 'term_relationships';
       
		foreach ( $terms as $row ) {
            if ( self::checkIfTermExists( $row['term_taxonomy_id'], $row['slug'], $taxonomy ) ) {
                $sql = $wpdb->prepare("INSERT INTO $table_trels "
                      . " (`object_id`, `term_taxonomy_id`, `term_order`) "
                      . " VALUES ( %d, %d, 0 ) ",
                      $post_id, $row['term_taxonomy_id']);
        
                $wpdb->query( $sql );
            }
            else { // add missing taxonomy term to report outwards
              
                // prepare output by changing it to array
                if ( $result === true ) {
                    $result = array();
                }
                
                $result[] = $row['slug'];
            }
		}
        
        return $result;
	}
    
    /**
     * Checks whether specified category/tag exists on the current subsite and matches by ID and slug
     * 
     * @param int $term_id
     * @param string $slug
     * @param string $taxonomy
     */
    protected static function checkIfTermExists( $term_id, $slug, $taxonomy = "post_tag" ) {
        global $wpdb;
      
        // if we are on subsite, $wpdb->prefix would be altered automatically by WP
        $table_ttxes = $wpdb->prefix . 'term_taxonomy';
        $table_terms = $wpdb->prefix . 'terms';
        
        $sql = $wpdb->prepare("SELECT count(*) AS count FROM $table_ttxes AS ttxes "
            . " LEFT JOIN $table_terms AS terms ON ttxes.term_taxonomy_id = terms.term_id "
            . " WHERE ttxes.term_taxonomy_id = %d AND terms.slug = %s AND ttxes.taxonomy = %s ",
            $term_id, 
            $slug, 
            $taxonomy
        );
        
        $result = $wpdb->get_row( $sql, ARRAY_A );
        
        return $result['count'];
    }
}
