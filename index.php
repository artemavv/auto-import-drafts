<?php
/*
Plugin Name: Auto Import Draft
Plugin URI: https://irani.im/wp-auto-upload-images.html
Description: Automatically import external post from drafts site.
Version: 4.2
Author: Ali Irani & Artem
Author URI: https://irani.im
Text Domain: auto-upload-images
Domain Path: /src/lang
License: GPLv2 or later
*/

if (!defined('ABSPATH')) exit();

define('WPAUI_DIR', dirname(__FILE__));

require 'src/functions.php';
require 'src/WpAutoImport.php';
require 'src/PostsExporter.php';
require 'src/PostsImporter.php';


$wp_aui = new WPAutoImport();

$wp_aui->run();
